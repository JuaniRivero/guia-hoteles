$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#reserva').on('show.bs.modal', function(e) {
        console.log('se muestra');
        $('#reservaBtn').removeClass('btn-primary');
        $('#reservaBtn').addClass('btn-success');
        $('#reservaBtn').prop('disabled', true);
    });
    $('#reserva').on('shown.bs.modal', function(e) {
        console.log('se mostro');
    });
    $('#reserva').on('hide.bs.modal', function(e) {
        console.log('se oculta');

    });
    $('#reserva').on('hidden.bs.modal', function(e) {
        console.log('se oculto');
        $('#reservaBtn').prop('disabled', false);
        $('#reservaBtn').removeClass('btn-success');
        $('#reservaBtn').addClass('btn-primary');
    });
});